<?php 

/**
* 
*/
class ComHighholidaysModelRegistrationselections extends KModelTable
{
    
    public function __construct(KConfig $config)
    {
        parent::__construct($config);
 
        $this->_state
            ->insert('registration_id', 'int')
            ;
    }

    protected function _buildQueryColumns(KDatabaseQuery $query)
    {
        parent::_buildQueryColumns($query);
        
        $query->select('selections.*');
        $query->select('tbl.amount AS sub_total');
    }

    protected function _buildQueryJoins(KDatabaseQuery $query)
    {
        parent::_buildQueryJoins($query);
        
        $query->join('LEFT', 'highholidays_selections AS selections', 'tbl.highholidays_selection_id=selections.highholidays_selection_id');
    }

    public function _buildQueryWhere(KDatabaseQuery $query)
    {
        $state = $this->_state;
        
        if ($state->registration_id) {
            $query->where('tbl.highholidays_registration_id', '=', $state->registration_id);
        }

        parent::_buildQueryWhere($query);
    }
}
