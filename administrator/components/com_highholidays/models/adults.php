<?php 

/**
* 
*/
class ComHighholidaysModelAdults extends KModelTable
{
    
    public function __construct(KConfig $config)
    {
        parent::__construct($config);
        
        $this->_state
            ->insert('registration_id', 'int')
            ;
    }

	protected function _buildQueryColumns(KDatabaseQuery $query)
    {
        parent::_buildQueryColumns($query);
        
        $query->select('registrations.first_name AS registrants_fname');
        $query->select('registrations.last_name AS registrants_lname');
    }

    protected function _buildQueryJoins(KDatabaseQuery $query)
    {
        parent::_buildQueryJoins($query);
        
        $query->join('LEFT', 'highholidays_registrations AS registrations', 'tbl.highholidays_registration_id=registrations.highholidays_registration_id');
    }

    protected function _buildQueryWhere(KDatabaseQuery $query)
    {
        $state = $this->_state;

        if ($state->registration_id) {
            $query->where('tbl.highholidays_registration_id', '=', $state->registration_id);
        }

        parent::_buildQueryWhere($query);
    }
}
