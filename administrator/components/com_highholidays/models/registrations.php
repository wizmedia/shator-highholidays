<?php 

/**
* 
*/
class ComHighholidaysModelRegistrations extends KModelTable
{
    
    public function __construct(KConfig $config)
    {
        parent::__construct($config);
        
        $this->_state
            ->insert('search', 'string')
            ->insert('filter_babysitting', 'int')
            ->insert('filter_yizkor', 'int')
            ->insert('filter_lulavetrog', 'int')
            ->insert('filter_volunteer', 'int')
            ->insert('filter_requestbill', 'int')
            ->insert('search_start_date', 'string')
            ->insert('search_end_date', 'string')
            ->insert('filter_layout', 'string')
            ->insert('filter_members', 'int')
            ->insert('filter_nonmembers', 'int')
            ;
    }

    protected function _buildQueryWhere(KDatabaseQuery $query)
    {
        $state = $this->_state;

        if ($state->filter_layout) {
            switch ($state->filter_layout) {
                case 'filter_babysitting':
                    $query->where('tbl.f_babysitting', '=', 1);
                break;
                case 'filter_yizkor':
                    $query->where('tbl.f_yizkor', '=', 1);
                break;
                case 'filter_volunteer':
                    $query->where('tbl.f_volunteer', '=', 1);
                break;
                case 'filter_lulavetrog':
                    $query->where('tbl.f_lulavetrog', '=', 1);
                break;
                case 'filter_members':
                    $query->where('tbl.ismember', '=', 1);
                break;
                case 'filter_nonmembers':
                    $query->where('tbl.ismember', '=', 0);
                break;
            }
        }
        
        if ($state->filter_babysitting)         $query->where('tbl.f_babysitting', '=', $state->filter_babysitting);
        if ($state->filter_yizkor)              $query->where('tbl.f_yizkor', '=', $state->filter_yizkor);
        if ($state->filter_volunteer)            $query->where('tbl.f_volunteer', '=', $state->filter_volunteer);
        if ($state->filter_lulavetrog)          $query->where('tbl.f_lulavetrog', '=', $state->filter_lulavetrog);
        if ($state->filter_requestbill)         $query->where('tbl.payment_method', '=', 'bill');

        if ($state->search) {
			$search = '%'.$state->search.'%';
            $query->where('CONCAT_WS(" ", tbl.first_name, tbl.last_name)', 'LIKE', $search);
        }

        if ($state->search_start_date) {
            $query->where('tbl.payment_date', '>=', $state->search_start_date.' 00:00:00');
        }

        if ($state->search_end_date) {
            $query->where('tbl.payment_date', '<=', $state->search_end_date.' 23:59:59');            
        }

        parent::_buildQueryWhere($query);
    }
}
