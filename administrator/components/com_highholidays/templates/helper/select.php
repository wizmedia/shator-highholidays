<?php

/**
* 
*/
class ComHighholidaysTemplateHelperSelect extends KTemplateHelperSelect
{
	public function yizkor_listing($config = array())
	{
		$config = new KConfig($config);
		$config->append(array(
			'name' 		=> 'yizkor_listing',
			'key' 		=> 'value',
			'text'		=> 'text',
			'attribs'	=> array(),
			'selected'	=> null,
			'prefix_id' => ''
		));
		
		$name = $config->name;
		$attribs = KHelperArray::toString($config->attribs);

		$types = array(
		        'same'      => JText::_('To be the same listing as last year'), 
		        'review'    => JText::_('To be emailed to me as it was last year so I can review it'), 
		        'new'       => JText::_('To be a New/Changed Listing (please enter information below)')
		        );
		
		$html = array();
		foreach ($types as $value => $text)
		{
			$selected = $config->selected == $value ? 'checked="checked"' : '';

			$html[] = '<input type="radio" name="'.$config->name.'" id="'.$config->prefix_id.$value.'" value="'.$value.'" '.$selected.' '.$attribs.' />';
			$html[] = '<label for="'.$config->prefix_id.$value.'">'.$text.'</label>';
		}

		return implode(PHP_EOL, $html);
	}
	
	public function paymentmethod($config = array())
	{
		$config = new KConfig($config);
		$config->append(array(
			'name' 		        => 'payment_method',
			'selected'	        => null,
			'prefix_id'	        => '',
			'default'	        => 'cc',
			'attribs'	        => array(),
			'key'		        => 'value',
			'text'		        => 'text',
            'show_checkcash'    => false
		));

		$types = array(
			'cc' 		=> JText::_('I would like to pay by Credit Card'),
			// 'bill'		=> JText::_('I would like Shaare Torah to bill me for the above amount.'),
			// 'check'		=> JText::_('I will send a check to Shaare Torah at P.O. Box 83598 Gaithersburg, MD 20883'),
			'checkcash' => JText::_('Check Cash'),
			'zero balance'	=> JText::_('I have a $0 balance'),
			'special payment plan' => JText::_(' I have a pre-approved payment arrangement through the Shaare Torah office.')
		);

		$attribs = KHelperArray::toString($config->attribs);

		$html = array();
		foreach ($types as $value => $text) {
			if (!$config->show_checkcash AND $value == 'checkcash') {
			    continue;
			}
			
			if (is_null($config->selected)) {
				$config->selected = $config->default;
			}

			$selected = $config->selected == $value ? 'checked="checked"' : '';

			$html[] = '<input type="radio" name="'.$config->name.'" id="'.$config->prefix_id.$value.'" value="'.$value.'" '.$selected.' '.$attribs.' />';
			$html[] = '<label for="'.$config->prefix_id.$value.'">'.$text.'</label>';
			$html[] = '<br />';
		}

		return implode(PHP_EOL, $html);		
	}
	
}
