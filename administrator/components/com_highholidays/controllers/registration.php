<?php 

/**
* 
*/
class ComHighholidaysControllerRegistration extends ComDefaultControllerDefault
{
    
    public function __construct(KConfig $options)
    {
        parent::__construct($options);

        $this
            ->registerCallback('before.add', array(KFactory::get('admin::com.highholidays.helper.registration'), 'validateRegistration'))
            ->registerCallback('before.add', array(KFactory::get('admin::com.highholidays.helper.registration'), 'registrationsSteps'))
            ->registerCallback('before.add', array(KFactory::get('admin::com.eventsdonations.helper.payment'), 'processPayment'))
            ->registerCallback('after.add', array(KFactory::get('admin::com.highholidays.helper.registration'), 'saveSelections'))
            ->registerCallback('after.add', array(KFactory::get('admin::com.highholidays.helper.registration'), 'saveAdults'))
            ->registerCallback('after.add', array(KFactory::get('admin::com.highholidays.helper.registration'), 'cleanSession'))
            ->registerCallback('after.add', array($this, 'overrideRedirect'))
            ->registerCallback('after.delete', array($this, 'deleteAdults'))
            ->registerCallback('after.delete', array($this, 'deleteSelections'))
            ;
    }

    public function overrideRedirect(KCommandContext $context)
    {
        $url = clone(KRequest::URL());
        $url->scheme = 'http';
        $url->query = array('option' => 'com_highholidays', 'view' => 'registrations');

        KFactory::get('lib.koowa.application')->redirect($url, 'Success');
    }

    public function _actionCancel(KCommandContext $context)
    {
        $identifier = (string)$context->caller->getIdentifier();
        
        // Unset sessions variable.
        KRequest::set('session.'.$identifier, null);
        
        $this->setRedirect('index.php?option=com_highholidays&view=registrations');
    }

    public function getRequest()
    {
        if (KInflector::isPlural($this->_request->view)) {
            if (!$this->_request->sort) {
                $this->_request->sort = 'payment_date';
                $this->_request->direction = 'desc';
            }            
        }

        return parent::getRequest();
    }

    public function deleteAdults(KCommandContext $context)
    {
        $registrations = $context->result;
        
        $adultstable = KFactory::tmp('admin::com.highholidays.database.table.adults');

        foreach ($registrations as $registration) {
            // Delete Adults
            $queryadults = $adultstable->getDatabase()
                            ->getQuery()
                            ->where('highholidays_registration_id', '=', $registration->id);
            $adultstable->select($queryadults)->delete();
        }
    }

    public function deleteSelections(KCommandContext $context)
    {
        $registrations = $context->result;
        
        $regselectionstable = KFactory::tmp('admin::com.highholidays.database.table.registrationselections');

        foreach ($registrations as $registration) {
            // Delete Selections
            $queryselections = $regselectionstable->getDatabase()
                                ->getQuery()
                                ->where('highholidays_registration_id', '=', $registration->id);
            $regselectionstable->select($queryselections)->delete();
        }
    }

    public function _actionRead(KCommandContext $context)
    {
       $url = KRequest::URL();
       $view = $this->getView();

       if ($view->getName() == 'registration' && $url->scheme != 'https' && $this->_request->layout != 'form' && !empty($this->_request->layout) ) {
           $url->scheme = 'https';

           KFactory::get('lib.koowa.application')->redirect($url);
       }else {
           return parent::_actionRead($context);
       }
    }
}
