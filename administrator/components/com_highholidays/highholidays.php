<?php

// Check if Koowa is active
if(!defined('KOOWA')) {
    JError::raiseWarning(0, JText::_("Koowa wasn't found. Please install the Koowa plugin and enable it."));
    return;
}
 
KFactory::get('lib.joomla.language')->load('com_eventsdonations');

// Create the controller dispatcher
echo KFactory::get('admin::com.highholidays.dispatcher')->dispatch(KRequest::get('get.view', 'cmd', 'registrations'));
