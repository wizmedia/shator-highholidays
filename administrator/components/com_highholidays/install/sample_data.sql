CREATE TABLE `shator_highholidays_adults` (
  `highholidays_adult_id` int(11) NOT NULL AUTO_INCREMENT,
  `highholidays_registration_id` int(11) NOT NULL,
  `first_name` varchar(32) NOT NULL,
  `last_name` varchar(32) NOT NULL,
  `address_1` varchar(100) NOT NULL DEFAULT '',
  `address_2` varchar(100) NOT NULL,
  `city` varchar(32) NOT NULL DEFAULT '',
  `state` varchar(32) NOT NULL DEFAULT '',
  `country` varchar(32) NOT NULL DEFAULT 'USA',
  `zip` varchar(32) NOT NULL DEFAULT '',
  `phone_1` varchar(32) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`highholidays_adult_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `shator_highholidays_registration_selections` (
  `highholidays_registration_selection_id` int(11) NOT NULL AUTO_INCREMENT,
  `highholidays_registration_id` int(11) NOT NULL,
  `highholidays_selection_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` float(10,2) NOT NULL,
  PRIMARY KEY (`highholidays_registration_selection_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `shator_highholidays_registrations` (
  `highholidays_registration_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(32) NOT NULL,
  `last_name` varchar(32) NOT NULL,
  `spousename` varchar(255) NOT NULL,
  `address_1` varchar(100) NOT NULL DEFAULT '',
  `address_2` varchar(100) NOT NULL,
  `city` varchar(32) NOT NULL DEFAULT '',
  `state` varchar(32) NOT NULL DEFAULT '',
  `country` varchar(32) NOT NULL DEFAULT 'USA',
  `zip` varchar(32) NOT NULL DEFAULT '',
  `phone_1` varchar(32) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mail_me` tinyint(1) NOT NULL,
  `ismember` tinyint(1) NOT NULL,
  `interested_to_join` tinyint(1) NOT NULL,
  `yizkor_listing` varchar(50) DEFAULT NULL,
  `yizkor_names_list` text NOT NULL,
  `yizkor_wishes` text NOT NULL,
  `volunteer` varchar(200) NOT NULL,
  `f_volunteer` tinyint(1) DEFAULT '0',
  `f_babysitting` tinyint(1) DEFAULT '0',
  `f_yizkor` tinyint(1) DEFAULT '0',
  `f_lulavetrog` tinyint(1) DEFAULT '0',
  `payment_date` datetime DEFAULT NULL,
  `paid` tinyint(1) DEFAULT '0',
  `recorded` tinyint(1) DEFAULT '0',
  `payment_method` varchar(20) DEFAULT NULL,
  `payment_log` text,
  `trans_id` int(11) DEFAULT NULL,
  `trans_result` text,
  `total_amount` float(10,2) DEFAULT NULL,
  `customer_note` text,
  `num_adult_member` int(11) NOT NULL DEFAULT '0',
  `num_adult_nonmember` int(11) NOT NULL DEFAULT '0',
  `num_staffteacher` int(11) NOT NULL DEFAULT '0',
  `num_youth_member` int(11) NOT NULL DEFAULT '0',
  `num_youth_nonmember` int(11) NOT NULL DEFAULT '0',
  `num_child_member` int(11) NOT NULL DEFAULT '0',
  `num_child_nonmember` int(11) NOT NULL DEFAULT '0',
  `num_babysitting` int(11) NOT NULL DEFAULT '0',
  `num_lulavetrog` int(11) NOT NULL DEFAULT '0',
  `num_yizkor` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`highholidays_registration_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;

CREATE TABLE `shator_highholidays_selections` (
  `highholidays_selection_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `extra_info` varchar(255) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `alt_amount` float(10,2) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  PRIMARY KEY (`highholidays_selection_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `shator_highholidays_selections` (`highholidays_selection_id`,`title`,`name`,`extra_info`,`amount`,`alt_amount`,`ordering`)
VALUES
	(1,'Adult Member Tickets','member-adult-tickets',NULL,0.00,NULL,1),
	(2,'Additional or Non-Member Adult Tickets','nonmember-adult-tickets',NULL,200.00,NULL,2),
	(3,'Staff/Teacher Tickets','staff-teacher-tickets',NULL,0.00,NULL,3),
	(4,'Member Youth Tickets','member-youth-tickets',NULL,0.00,NULL,4),
	(5,'Non-Member Youth Tickets','nonmember-youth-tickets',NULL,75.00,NULL,5),
	(6,'Member Children\'s Ticket','member-child-tickets',NULL,0.00,NULL,6),
	(7,'Non-Member Children\'s Tickets','nonmember-child-tickets',NULL,30.00,NULL,7),
	(8,'On Site Babysitting Services','babysitting-services',NULL,35.00,NULL,8),
	(9,'Yizkor Book Listing Donation','yizkor-book-listing',NULL,54.00,108.00,9),
	(10,'Lulav & Etrog Set','lulav-etrog',NULL,40.00,NULL,10);