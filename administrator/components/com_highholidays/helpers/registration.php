<?php 

/**
* 
*/
class ComHighholidaysHelperRegistration extends KObject
{
    
    public function validateRegistration(KCommandContext $context)
    {
        $identifier = (string)$context->caller->getIdentifier();
        $data = KRequest::get('session.'.$identifier, 'raw', array());

        if (count(KConfig::toData($context->data->orderselections)))    unset($data['orderselections']); // Avoid merge conflict of orderselections(array)
        if (count(KConfig::toData($context->data->adults)))   unset($data['adults']);

        $return = TRUE;
        switch ($context->data->step)
        {
            case 'step1':
                if (!$this->validateUserDetails($context))      $return = FALSE;
                unset($data['volunteer']);
            break;
            case 'step2':
                unset($data['orderselections']);
            break;
            case 'step3':
                if (!KFactory::get('admin::com.eventsdonations.validation.transaction')->validatePaymentDetails($context))    $return = FALSE;
            break;
        }

        $context->data->append($data);
        KRequest::set('session.'.$identifier, KConfig::toData($context->data));

        if (!$return) {
            return FALSE;
        }
    }
    
    public function validateQuantity(KCommandContext $context)
    {
        $app = KFactory::get('lib.joomla.application');
        $selections = KFactory::tmp('admin::com.highholidays.model.selections')->getList();
  
	
		foreach($selections as $selection){
				$context->data->orderselections[$selection->name] = (int)$context->data->orderselections[$selection->name];
	    }
	
		$orderselections = $context->data->orderselections;
      
  		if($orderselections['staff-teacher-tickets'] > 2){
            $app->enqueueMessage('Staff/Teacher tickets exceeded the limit.', 'error');
            return FALSE;
        }
        if($orderselections['member-adult-tickets'] > 2){
            $app->enqueueMessage('Adult member tickets exceeded the limit.', 'error');
            return FALSE;
        }
        
     }

    public function registrationsSteps(KCommandContext $context)
    {
        $identifier = (string)$context->caller->getIdentifier();

        switch ($context->data->step)
        {
            case 'step1':
                $context->caller->setRedirect(JRoute::_('index.php?option=com_highholidays&view=registration&layout=step2', FALSE));
                return FALSE;
            break;
            case 'step2':
                $context->caller->setRedirect(JRoute::_('index.php?option=com_highholidays&view=registration&layout=step3', FALSE));
                return FALSE;
            break;
        }
    }

    public function cleanSession(KCommandContext $context)
    {
		$identifier = (string)$context->caller->getIdentifier();

        KRequest::set('session.'.$identifier, null);
    }

    public function validateUserDetails(KCommandContext $context)
    {
        $app = KFactory::get('lib.joomla.application');
        $return = TRUE;
        
        if (empty($context->data->first_name))  $return = FALSE;
        if (empty($context->data->last_name))   $return = FALSE;
        if (empty($context->data->email) OR !KFactory::get('lib.koowa.filter.email')->validate($context->data->email))  $return = FALSE;
        if (empty($context->data->address_1))   $return = FALSE;
        if (empty($context->data->city))        $return = FALSE;
        if (empty($context->data->state))       $return = FALSE;
        if (empty($context->data->zip))         $return = FALSE;
        if (empty($context->data->phone_1))       $return = FALSE;
        if (empty($context->data->area_code))       $return = FALSE;

        if (!$return) {
            $app->enqueueMessage('Please fill in all the required fields.', 'error');
        }

        return $return;
    }

    public function saveSelections(KCommandContext $context)
    {
        $selections = KFactory::tmp('admin::com.highholidays.model.selections')->getList();
        $orderselections = $context->data->orderselections;

        foreach ($selections as $selection) {
            if (isset($orderselections[$selection->name])) {
                if ($quantity = $orderselections[$selection->name]) {
                    $amount = $quantity * $selection->amount;
                    $price = $selection->amount;

                    $data = array(
                        'highholidays_registration_id' => $context->result->id,
                        'highholidays_selection_id' => $selection->id,
                        'quantity' => $quantity,
                        'amount' => $amount
                    );

                    $selection = KFactory::tmp('admin::com.highholidays.database.row.registrationselection');
                    $selection->setData($data);
                    $selection->save();
                }
            }
        }
    }

    public function saveAdults(KCommandContext $context)
    {
        $adults = $context->data->adults;
        
        foreach ($adults as $data) {
            $data = KConfig::toData($data);
            $data['highholidays_registration_id'] = $context->result->id;

            $adult = KFactory::tmp('admin::com.highholidays.database.row.adult');
            $adult->setData($data);
            $adult->save();
        }
    }

    public function sendMailNotification(KCommandContext $context)
    {
        $html = KFactory::tmp('admin::com.highholidays.view.email.html')
                    ->layout('registration')
                    ->assign('registration', $context->result)
                    ->assign('mailToCustomer', TRUE)
                    ->display();

        $config = new KConfig;
        $config->subject = 'High Holidays Registration';
        $config->recipient = $context->data->email;
        $config->body = $html;

        $this->sendnotification($config);
    }

    public function sendAdminMailNotification(KCommandContext $context)
    {
        $params = JComponentHelper::getParams('com_highholidays');

        $html = KFactory::tmp('admin::com.highholidays.view.email.html')
                    ->layout('registration')
                    ->assign('registration', $context->result)
                    ->assign('mailToCustomer', FALSE)
                    ->display();

        $config = new KConfig;
        $config->subject = 'High Holidays Registration';
        $config->recipient = $params->get('notification_email_to');
        $config->body = $html;

        $this->sendnotification($config);
    }
    

    public function sendnotification($config)
    {
        $mailer =& JFactory::getMailer();

		// Build e-mail message format
		//$mailer->setSender($config->from, $config->fromname);
		$mailer->setSubject($config->subject);
		$mailer->setBody($config->body);
		$mailer->IsHTML(TRUE);
        
        $recipient = KConfig::toData($config->recipient);
        
		$mailer->addRecipient($recipient);
        
        if ($config->ccTo) {
            $mailer->addCC(explode(',', $config->ccTo));
        }

		// Send the Mail
		$rs	= $mailer->Send();

		// Check for an error
		if (JError::isError($rs))
		{

		}
    }
}
