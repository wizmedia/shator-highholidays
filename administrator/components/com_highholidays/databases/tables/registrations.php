<?php

/**
* 
*/
class ComHighholidaysDatabaseTableRegistrations extends KDatabaseTableDefault
{
	public function __construct(KConfig $config)
	{
		parent::__construct($config);
	}
	
	public function _initialize(KConfig $config)
	{
		$config->name = 'highholidays_registrations';
		$config->base = 'highholidays_registrations';

		parent::_initialize($config);
	}
}

