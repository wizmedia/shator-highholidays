<?php

/**
* 
*/
class ComHighholidaysDatabaseTableSelections extends KDatabaseTableDefault
{
	public function __construct(KConfig $config)
	{
		parent::__construct($config);
	}
	
	public function _initialize(KConfig $config)
	{
		$config->name = 'highholidays_selections';
		$config->base = 'highholidays_selections';

		parent::_initialize($config);
	}
}

