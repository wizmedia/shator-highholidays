<?php 

/**
* 
*/
class ComHighholidaysDatabaseRowRegistration extends KDatabaseRowDefault
{
    
    public function save()
    {
        if (empty($this->_data['payment_date'])) {
            $this->_data['payment_date'] = gmdate('Y-m-d H:i:s');
        }

        if (isset($this->_data['volunteer'])) {
            if (is_array($this->_data['volunteer'])) {
                $this->_data['volunteer'] = implode(',', $this->_data['volunteer']);

                if (!empty($this->_data['volunteer'])) {
                    $this->_modified['f_volunteer'] = 1;
                    $this->_data['f_volunteer'] = 1;                    
                }
            }
        }
        
        if (isset($this->_data['yizkor_listing'])) {
            $this->_modified['f_yizkor'] = 1;
            $this->_data['f_yizkor'] = 1;
        }

        if (isset($this->_data['orderselections'])) {
            foreach ($this->_data['orderselections'] as $selection => $num_tickets) {
                if (!empty($num_tickets)) {
                    switch ($selection) {
                        case 'lulav-etrog':
                            $this->_modified['f_lulavetrog'] = 1;
                            $this->_data['f_lulavetrog'] = 1;

                            $this->_modified['num_lulavetrog'] = 1;     
                            $this->_data['num_lulavetrog'] = $this->_data['orderselections']['lulav-etrog'];
                        break;
                        case 'babysitting-services':
                            $this->_modified['f_babysitting'] = 1;
                            $this->_data['f_babysitting'] = 1;

                            $this->_modified['num_babysitting'] = 1;     
                            $this->_data['num_babysitting'] = $this->_data['orderselections']['babysitting-services'];
                        break;
                        case 'yizkor-book-listing':
                            $this->_modified['f_yizkor'] = 1;
                            $this->_data['f_yizkor'] = 1;

                            $this->_modified['num_yizkor'] = 1;     
                            $this->_data['num_yizkor'] = $this->_data['orderselections']['yizkor-book-listing'];
                        break;
                        case 'member-adult-tickets':
                            $this->_modified['num_adult_member'] = 1;
                            $this->_data['num_adult_member'] = $num_tickets;
                        break;
                        case 'nonmember-adult-tickets':
                            $this->_modified['num_adult_nonmember'] = 1;
                            $this->_data['num_adult_nonmember'] = $num_tickets;
                        break;
                        case 'staff-teacher-tickets':
                            $this->_modified['num_staffteacher'] = 1;
                            $this->_data['num_staffteacher'] = $num_tickets;
                        break;
                        case 'member-youth-tickets':
                            $this->_modified['num_youth_member'] = 1;
                            $this->_data['num_youth_member'] = $num_tickets;
                        break;
                        case 'nonmember-youth-tickets':
                            $this->_modified['num_youth_nonmember'] = 1;
                            $this->_data['num_youth_nonmember'] = $num_tickets;
                        break;
                        case 'member-child-tickets':
                            $this->_modified['num_child_member'] = 1;
                            $this->_data['num_child_member'] = $num_tickets;
                        break;
                        case 'nonmember-child-tickets':
                            $this->_modified['num_child_nonmember'] = 1;
                            $this->_data['num_child_nonmember'] = $num_tickets;
                        break;
                    }
                }
            }
        }

        return parent::save();
    }
}
