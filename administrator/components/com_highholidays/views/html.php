<?php 

/**
* 
*/
class ComHighholidaysViewHtml extends ComDefaultViewHtml
{
   public function __construct(KConfig $config)
   {
       $config->views = array(
            'registrations' => 'Registrations'
       );

        parent::__construct($config);
   }
}
