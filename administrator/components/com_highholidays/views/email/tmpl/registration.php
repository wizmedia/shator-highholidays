<? defined('KOOWA') or die('Restricted access');?>

<? if ($mailToCustomer): ?>
<p><?= @text('Thank you for registering for this event.'); ?>
<? endif; ?>

<h3><?= @text('Registration Details'); ?></h3>
<p><?= @text('Registered Date'); ?>: <?= $registration->payment_date; ?></p>
<p><?= @text('Subscribe to newsletter'); ?>: <strong><?= $registration->mail_me ? 'Yes' : 'No'; ?></strong></p>
<p><?= @text('I am a member'); ?>: <strong><?= $registration->ismember ? 'Yes' : 'No';  ?></strong></p>
<p><?= @text('I am interested in learning more about joining Shaare Torah'); ?>: <strong><?= $registration->interested_to_join ? 'Yes' : 'No'; ?></strong></p> 

<h3><?= @text('Additional Addults'); ?></h3>
<?= @template('admin::com.highholidays.view.registration.listadults', array('registration' => $registration)); ?>

<? if ($registration->f_yizkor): ?>
<h3><?= @text('Yizkor Book Listing'); ?></h3>
<p>I would like my listing: <?= $registration->yizkor_listing ? @text('YIZKOR_LISTING_'.$registration->yizkor_listing) : 'no'; ?></p>
<p>Names on list: <?= $registration->yizkor_names_list; ?></p>
<p>Wishes to remember: <?= $registration->yizkor_wishes; ?></p>
<? endif; ?>

<? if ($registration->f_volunteer): ?>
<? $volunteers = explode(',', $registration->volunteer); ?>
<h3><?= @text('Volunteer Opportunities'); ?></h3>
<p>I am interested in Volunteering to: </p>
    <ul>
    <? foreach ($volunteers as $volunteer): ?>
        <li><?= @text('VOLUNTEER_'.$volunteer); ?></li>
    <? endforeach; ?>                
    </ul>
<? endif; ?>

<h3><?= @text('Order Selections'); ?></h3>
<?= @template('admin::com.highholidays.view.registration.listselections', array('registration' => $registration)); ?>

<h3><?= @text('Customer\'s Note'); ?></h3>
<p><?= $registration->customer_note; ?></p>

<?= @template('admin::com.eventsdonations.view.list.customer_information', array('transaction' => $registration)); ?>
<?= @template('admin::com.eventsdonations.view.list.payment_information', array('transaction' => $registration)); ?>