<?php 

/**
* 
*/
class ComHighholidaysViewRegistrationHtml extends ComHighholidaysViewHtml
{
    
    public function display()
    {
        $toolbar = KFactory::get('admin::com.highholidays.toolbar.registration')
            ->reset()
            ->append('cancel');

		$data = KRequest::get('session.admin::com.highholidays.controller.registration', 'raw', array());
        $data = new KConfig($data);

        $this->assign('data', $data);

        $selections = KFactory::tmp('admin::com.highholidays.model.selections')->getList();
        $this->assign('selections', $selections);

        $layout = $this->getLayout();

        if ($layout == 'form') {
            $toolbar->prepend('save');
        }

        return parent::display();
    }
}
