<? defined('KOOWA') or die('Restricted access');?>

<? $adults = KFactory::tmp('admin::com.highholidays.model.adults')->set('registration_id', $registration->id)->getList(); ?>

<? $i = 1;?>
<? foreach ($adults as $adult): ?>
    <h4>Adult #<?= $i;?></h4>
    <p><?= @text('First name'); ?>: <?= $adult->first_name; ?></p>
    <p><?= @text('Last name'); ?>: <?= $adult->last_name; ?></p>
    <p><?= @text('Address'); ?>: <?= $adult->address_1; ?></p>
    <p><?= @text('City'); ?>: <?= $adult->city; ?></p>
    <p><?= @text('State'); ?>: <?= $adult->state; ?></p>
    <p><?= @text('Zip'); ?>: <?= $adult->zip; ?></p>
    <p><?= @text('Country'); ?>: <?= $adult->country; ?></p>
    <p><?= @text('Phone'); ?>: <?= $adult->phone_1; ?></p>
    <p><?= @text('Email'); ?>: <?= $adult->email; ?></p>
    <? $i++; ?>
<? endforeach; ?>