<? defined('KOOWA') or die('Restricted access');?>

<? $selections = KFactory::tmp('admin::com.highholidays.model.registrationselections')->registration_id($registration->id)->getList(); ?>

<table class="adminlist" width="100%">
    <tr>
        <th style="text-align:left;"><?= @text('Title'); ?></th>
        <th style="text-align:left;"><?= @text('Quantity'); ?></th>
        <th style="text-align:left;"><?= @text('Sub Total'); ?></th>
    </tr>
    <? $total = 0; ?>
    <? foreach ($selections as $selection):?>
        <tr>
            <td><?= $selection->title; ?></td>
            <td><?= $selection->quantity; ?></td>
            <td>$<?= $selection->sub_total; ?></td>
        </tr>
    <? endforeach; ?>
    <tr>
        <td colspan="2" align="right"><strong><?= @text('Total'); ?>:</strong> </td>
        <td>$<?= $registration->total_amount; ?></td>
    </tr>
</table>