<? defined('KOOWA') or die('Restricted access');?>

<script src="media://lib_koowa/js/koowa.js" />

<form action="<?= @route('id='.$registration->id);?>" method="post" name="adminForm" id="adminForm">
    <fieldset class="adminform">
        <legend><?= @text('Details'); ?></legend>
        
        <div class="col width-50">
            <h3><?= @text('Registration'); ?></h3>

            <p><?= @text('Registration Id'); ?>: <?= $registration->id; ?></p>
            <p><?= @text('Date'); ?>: <?= $registration->payment_date; ?></p>
            <p><?= @text('Spouse name'); ?>: <?= $registration->spousename; ?></p>
            <p><?= @text('Subscribe to newsletter'); ?>: <strong><?= $registration->mail_me ? 'Yes' : 'No'; ?></strong></p>
            <p><?= @text('I am a member'); ?>: <strong><?= $registration->ismember ? 'Yes' : 'No';  ?></strong></p>
            <p><?= @text('I am interested in learning more about joining Shaare Torah'); ?>: <strong><?= $registration->interested_to_join ? 'Yes' : 'No'; ?></strong></p> 
            <p><?= @text('Mark as paid'); ?>: <?= @helper('select.booleanlist', array('name' => 'paid', 'selected' => $registration->paid)); ?></p>
            
            <h3><?= @text('Additional Addults'); ?></h3>
            <?= @template('listadults'); ?>
            
            <? if ($registration->f_yizkor): ?>
            <h3><?= @text('Yizkor Book Listing'); ?></h3>
            <p>I would like my listing: <?= $registration->yizkor_listing ? @text('YIZKOR_LISTING_'.$registration->yizkor_listing) : 'no'; ?></p>
            <p>Names on list: <?= $registration->yizkor_names_list; ?></p>
            <p>Wishes to remember: <?= $registration->yizkor_wishes; ?></p>
            <? endif; ?>
            
            <? if ($registration->f_volunteer): ?>
            <? $volunteers = explode(',', $registration->volunteer); ?>
            <h3><?= @text('Volunteer Opportunities'); ?></h3>
            <p>I am interested in Volunteering to: </p>
                <ul>
                <? foreach ($volunteers as $volunteer): ?>
                    <li><?= @text('VOLUNTEER_'.$volunteer); ?></li>
                <? endforeach; ?>                
                </ul>
            <? endif; ?>

            <h3><?= @text('Order Selections'); ?></h3>
            <?= @template('listselections'); ?>
            
            <?= @template('admin::com.eventsdonations.view.list.payment_information', array('transaction' => $registration, 'show_payment_logs' => TRUE)); ?>

            <h3><?= @text('Customer\'s Note'); ?></h3>
            <p><?= $registration->customer_note; ?></p>
        </div>

        <div class="col width-50">
            <?= @template('admin::com.eventsdonations.view.list.customer_information', array('transaction' => $registration)); ?>
        </div>

    </fieldset>
</form>