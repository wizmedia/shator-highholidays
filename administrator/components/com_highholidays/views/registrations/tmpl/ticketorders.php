<? defined('KOOWA') or die('Restricted access');?>

<table class="adminlist">
	<thead>
		<tr>
			<th width="5"><?= @text('Num'); ?></th>
			<th width="5"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?= count($registrations); ?>);" /></th>
			<th><?= @helper('grid.sort', array('column' => 'name', 'title' => 'Name of Registrant')); ?></th>
			<th><?= @helper('grid.sort', array('column' => 'email')); ?></th>
			<th><?= @helper('grid.sort', array('column' => 'num_adult_member', 'title' => '# Adult Member')); ?></th>
			<th><?= @helper('grid.sort', array('column' => 'num_adult_nonmember', 'title' => '# Adult Non-Member')); ?></th>
			<th><?= @helper('grid.sort', array('column' => 'num_staffteacher', 'title' => '# Staff / Teacher')); ?></th>
			<th><?= @helper('grid.sort', array('column' => 'num_youth_member', 'title' => '# Youth Member')); ?></th>
			<th><?= @helper('grid.sort', array('column' => 'num_youth_nonmember', 'title' => '# Youth Non-Member')); ?></th>
			<th><?= @helper('grid.sort', array('column' => 'num_child_member', 'title' => '# Child Member')); ?></th>
			<th><?= @helper('grid.sort', array('column' => 'num_child_nonmember', 'title' => '# Child Non-Member')); ?></th>
			<th><?= @helper('grid.sort', array('column' => 'num_babysitting', 'title' => '# Babysitting')); ?></th>
            <th></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="15">
				<?= @helper('paginator.pagination', array('total' => $total)); ?>
			</td>
		</tr>
	</tfoot>
	<tbody>
		<? $i = 0; $m = 0; ?>
		<? foreach($registrations as $registration): ?>
		<tr class="row<?= $m; ?>">
			<td align="center">
				<?= $i + 1;?>
			</td>
			<td align="center">
				<?= @helper('grid.checkbox', array('row' => $registration)); ?>
			</td>
			<td align="left">
                <?= $registration->first_name.' '.$registration->last_name; ?>
			</td>
            <td>
                <?= $registration->email; ?>
            </td>

            <td><?= $registration->num_adult_member; ?></td>
            <td><?= $registration->num_adult_nonmember; ?></td>
            <td><?= $registration->num_staffteacher; ?></td>
            <td><?= $registration->num_youth_member; ?></td>
            <td><?= $registration->num_youth_nonmember; ?></td>
            <td><?= $registration->num_child_member; ?></td>
            <td><?= $registration->num_child_nonmember; ?></td>
            <td><?= $registration->num_babysitting; ?></td>
            <td align="center">
                <a href="<?= @route('view=registration&id='.$registration->id); ?>"><?= @text('View'); ?></a>
            </td>
		</tr>
		<? $i = $i + 1; $m = (1 - $m);?>
		<? endforeach; ?>
	</tbody>
</table>