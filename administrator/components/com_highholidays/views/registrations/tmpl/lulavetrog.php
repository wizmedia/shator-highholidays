<? defined('KOOWA') or die('Restricted access');?>

<table class="adminlist">
	<thead>
		<tr>
			<th width="5"><?= @text('Num'); ?></th>
			<th width="5"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?= count($registrations); ?>);" /></th>
			<th><?= @helper('grid.sort', array('column' => 'name', 'title' => 'Name of Registrant')); ?></th>
			<th><?= @helper('grid.sort', array('column' => 'email')); ?></th>
            <th><?= @helper('grid.sort', array('column' => 'num_lulavetrog', 'title' => 'Number of Sets Ordered')); ?></th>            
			<th><?= @helper('grid.sort', array('column' => 'payment_method', 'title' => 'Payment Method')); ?></th>
            <th></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="10">
				<?= @helper('paginator.pagination', array('total' => $total)); ?>
			</td>
		</tr>
	</tfoot>
	<tbody>
		<? $i = 0; $m = 0; ?>
		<? foreach($registrations as $registration): ?>
		<tr class="row<?= $m; ?>">
			<td align="center">
				<?= $i + 1;?>
			</td>
			<td align="center">
				<?= @helper('grid.checkbox', array('row' => $registration)); ?>
			</td>
			<td align="left">
                <?= $registration->first_name.' '.$registration->last_name; ?>
			</td>
            <td>
                <?= $registration->email; ?>
            </td>
            <td>
                <?= $registration->num_lulavetrog; ?>
            </td>
            <td>
                <?= $registration->payment_method; ?>
            </td>
            <td align="center">
                <a href="<?= @route('view=registration&id='.$registration->id); ?>"><?= @text('View'); ?></a>
            </td>
		</tr>
		<? $i = $i + 1; $m = (1 - $m);?>
		<? endforeach; ?>
	</tbody>
</table>