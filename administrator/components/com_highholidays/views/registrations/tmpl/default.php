<? defined('KOOWA') or die('Restricted access');?>

<?= @helper('behavior.tooltip');?>

<script src="media://lib_koowa/js/koowa.js" />
<style src="media://com_default/css/admin.css" />
<style src="media://com_highholidays/css/default.css" />

<? $token = JUtility::getToken(); ?>
<script>
window.addEvent('domready', function() {
    function recordPayments(item) {
        var ischecked = Number(item.checked);
        var payment_id = item.value

        if (payment_id) {
            url = 'index.php?option=com_highholidays&view=registration&id=' + payment_id;
            var recordAjax = new Ajax(url, {
                    method: 'post',
                    data: { action: 'edit', recorded: ischecked, _token : '<?= $token;?>' }
                }).request();            
        }
    }

    $$('.record-registrations').addEvent('click', function() {
        recordPayments(this);
    });

//----------------------------------------------------------------------------------------------

	$$('.layouts').addEvent('click', function() {
   		var anchor_title = this.getProperty('title');
   		$('filter').setProperty('value', anchor_title);
   		$('adminForm').submit();
	});
	
	
});
</script>

<form action="<?= @route(); ?>" method="get" name="adminForm" id="adminForm">
    <fieldset>
        <legend>Filter</legend>	
		<input type="hidden" value="" name="filter_layout" id="filter">
		<a href="#" title="filter_all" id="layout" class="layouts <?= $state->filter_layout == 'filter_all' ? 'active' : '';?>" >All</a>
		<a href="#" title="filter_babysitting" id="layout" class="layouts <?= $state->filter_layout == 'filter_babysitting' ? 'active' : '';?>" >Babysitting Orders</a>
		<a href="#" title="filter_yizkor" id="layout" class="layouts <?= $state->filter_layout == 'filter_yizkor' ? 'active' : '';?>" >Yizkor Listing</a>
		<a href="#" title="filter_lulavetrog" id="layout" class="layouts <?= $state->filter_layout == 'filter_lulavetrog' ? 'active' : '';?>" >Lulav &amp; Etrog Orders</a>
		<a href="#" title="filter_volunteer" id="layout" class="layouts <?= $state->filter_layout == 'filter_volunteer' ? 'active' : '';?>" >Volunteers</a>
		<a href="#" title="filter_ticketorders" id="layout" class="layouts <?= $state->filter_layout == 'filter_ticketorders' ? 'active' : '';?>" >Ticket Orders</a>
		<a href="#" title="filter_billingdetails" id="layout" class="layouts <?= $state->filter_layout == 'filter_billingdetails' ? 'active' : '';?>" >Billing Details</a>
		<a href="#" title="filter_members" id="layout" class="layouts <?= $state->filter_layout == 'filter_members' ? 'active' : '';?>" >Member</a>
		<a href="#" title="filter_nonmembers" id="layout" class="layouts <?= $state->filter_layout == 'filter_nonmembers' ? 'active' : '';?>" >Non-Member</a>
    	<a href="#" title="filter_adults" id="layout" class="layouts <?= $state->filter_layout == 'filter_adults' ? 'active' : '';?>" >Adults</a>
	</fieldset>
	
    <table class="adminlist">
    	<thead>
            <tr>
                <td colspan="3">
                    <?= @text('Search'); ?>
                    <?= @template('admin::com.default.view.list.search_form'); ?>
                </td>
                <td colspan="7" align="right">
                    <?= @template('admin::com.eventsdonations.view.list.search_date'); ?>
                </td>
            </tr>
        </thead>
    </table>

    <? $layout = str_replace('filter_', '', $state->filter_layout); ?>
    <? $layout = $layout == 'all' || empty($layout) ? 'basicdetails' : $layout; ?>
    <?= @template($layout); ?>
</form>