<? defined('KOOWA') or die('Restricted access');?>

<table class="adminlist">
	<thead>
		<tr>
			<th width="5"><?= @text('Num'); ?></th>
			<th width="5"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?= count($registrations); ?>);" /></th>
			<th><?= @helper('grid.sort', array('column' => 'name', 'title' => 'Name of Registrant')); ?></th>
			<th><?= @helper('grid.sort', array('column' => 'email')); ?></th>
			<th><?= @helper('grid.sort', array('column' => 'phone', 'title' => 'Phone Number')); ?></th>
			<th><?= @helper('grid.sort', array('column' => 'payment_method', 'title' => 'Payment Method')); ?></th>
			<th><?= @helper('grid.sort', array('column' => 'total_amount', 'title' => 'Payment Amount')); ?></th>
			<th><?= @helper('grid.sort', array('column' => 'payment_date', 'title' => 'Payment Date')); ?></th>
            <th><?= @text('Paid'); ?></th>
            <th></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="10">
				<?= @helper('paginator.pagination', array('total' => $total)); ?>
			</td>
		</tr>
	</tfoot>
	<tbody>
		<? $i = 0; $m = 0; ?>
		<? foreach($registrations as $registration): ?>
		<tr class="row<?= $m; ?>">
			<td align="center">
				<?= $i + 1;?>
			</td>
			<td align="center">
				<?= @helper('grid.checkbox', array('row' => $registration)); ?>
			</td>
			<td align="left">
                <?= $registration->first_name.' '.$registration->last_name; ?>
			</td>
            <td>
                <?= $registration->email; ?>
            </td>
            <td>
                <?= ($registration->area_code ? $registration->area_code.'-' : '').$registration->phone_1; ?>
            </td>
            <td>
                <?= $registration->payment_method; ?>
            </td>
            <td>
                <?= $registration->total_amount; ?>
            </td>
            <td>
                <?= $registration->payment_date; ?>
            </td>
            <td>
                <?= $registration->paid ? 'Yes' : 'No' ?>
            </td>
            <td align="center">
                <a href="<?= @route('view=registration&id='.$registration->id); ?>"><?= @text('View'); ?></a>
            </td>
		</tr>
		<? $i = $i + 1; $m = (1 - $m);?>
		<? endforeach; ?>
	</tbody>
</table>