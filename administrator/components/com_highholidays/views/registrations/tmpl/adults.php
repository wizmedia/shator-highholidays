<? defined('KOOWA') or die('Restricted access');?>

<table class="adminlist">
	<thead>
		<tr>
			<th width="5"><?= @text('Num'); ?></th>
			<th width="5"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?= count($adults); ?>);" /></th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Address 1</th>
			<th>Address 2</th>
			<th>City</th>
			<th>State</th>
			<th>Country</th>
			<th>Zip</th>
			<th>Phone No.</th>
			<th>E-mail</th>
			<th>Registrants Name</th>
		</tr>
	</thead>
	<tbody>
		<? $i = 0; $m = 0; ?>
		<? foreach($adults as $adult): ?>
		<tr class="row<?= $m; ?>">
			<td align="center">
				<?= $i + 1;?>
			</td>
			<td align="center">
				<?= @helper('grid.checkbox', array('row' => $adult)); ?>
			</td>
			<td>
		        <?= $adult->first_name; ?>
			</td>
			<td>
		        <?= $adult->last_name; ?>
			</td>
			<td>
		        <?= $adult->address_1; ?>
			</td>
			<td>
		        <?= $adult->address_2; ?>
			</td>
			<td>
		        <?= $adult->city; ?>
			</td>
			<td>
		        <?= $adult->state; ?>
			</td>
			<td>
		        <?= $adult->country; ?>
			</td>
			<td>
		        <?= $adult->zip; ?>
			</td>
			<td>
		        <?= $adult->phone_1; ?>
			</td>
			<td>
		        <?= $adult->email; ?>
			</td>
			<td>
		        <?= $adult->registrants_fname.' '.$adult->registrants_lname; ?>
			</td>
		</tr>
		<? $i = $i + 1; $m = (1 - $m);?>
		<? endforeach; ?>
	</tbody>
</table>
