<?php 

/**
* 
*/
class ComHighholidaysViewRegistrationsHtml extends ComHighholidaysViewHtml
{
    
    public function display()
    {
        $toolbar = $this->getToolbar()->reset();

        $toolbar
            ->append(KFactory::get('admin::com.highholidays.toolbar.button.register'))
            ->append('delete')
            ->append(KFactory::get('admin::com.eventsdonations.toolbar.button.csv'))
            ->append(KFactory::get('admin::com.highholidays.toolbar.button.config'))
            ;

 		$adults = KFactory::get('admin::com.highholidays.model.adults')->getList();

		$this->assign('adults', $adults);

        return parent::display();
    }
}
