<?php

/**
* 
*/
class ComHighholidaysViewRegistrationsCsv extends KViewCsv
{
	public function display()
	{
        $model = $this->getModel();
        $state = $model->getState();

		//Get the rowset
		$rowset = $model->getList();

		
        // Header
        switch ($state->filter_layout) {
            case 'filter_babysitting':
                $this->filename = 'Babysitting_Orders.csv';
		        $this->output .= $this->_arrayToString(array('first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'state', 'country', 'zip', 'phone_number', 'number_of_babysitting_tickets_ordered', 'payment_method', 'status')).$this->eol;
            break;
            case 'filter_members':
                $this->filename = 'Members.csv';
		        $this->output .= $this->_arrayToString(array('first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'state', 'country', 'zip', 'phone_number', 'status')).$this->eol;
            break;
            case 'filter_nonmembers':
                $this->filename = 'Non_Members.csv';
		        $this->output .= $this->_arrayToString(array('first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'state', 'country', 'zip', 'phone_number', 'status')).$this->eol;
            break;
            case 'filter_yizkor':
                $this->filename = 'Yizkor_Listing.csv';
    	        $this->output .= $this->_arrayToString(array('first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'state', 'country', 'zip', 'phone_number', 'listing_preference', 'names_list', 'wishes_to_remember', 'payment_method', 'status')).$this->eol;
            break;
            case 'filter_lulavetrog':
                $this->filename = 'Lulav_and_Etrog_Orders.csv';
    	        $this->output .= $this->_arrayToString(array('first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'state', 'country', 'zip', 'phone_number', 'number_of_sets_ordered', 'payment_method', 'status')).$this->eol;
            break;
            case 'filter_volunteer':
                $this->filename = 'Volunteers.csv';
    	        $this->output .= $this->_arrayToString(array('first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'state', 'country', 'zip', 'phone_number', 'volunteering_for', 'status')).$this->eol;
            break;
            case 'filter_ticketorders':
                $this->filename = 'Ticket_Orders.csv';
    	        $this->output .= $this->_arrayToString(array('first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'state', 'country', 'zip', 'adult_member', 'adult_nonmember', 'staff_and_teacher', 'youth_member', 'youth_nonmember', 'child_member', 'child_nonmember', 'babysitting', 'lulav_etrog','yizkor', 'status')).$this->eol;
            break;
            case 'filter_billingdetails':
                $this->filename = 'Billing_Details.csv';
		        $this->output .= $this->_arrayToString(array('first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'state', 'country', 'zip', 'phone_number', 'payment_amount', 'payment_method', 'registered_date', 'paid', 'status')).$this->eol;            
            break;
            case 'filter_adults':
                $this->filename = 'Adults.csv';
		        $this->output .= $this->_arrayToString(array('first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'state', 'country', 'zip', 'phone_number', 'registrants name')).$this->eol;
            break;
            case 'filter_all':
			$this->filename = 'Registrants.csv';
		        $this->output .= $this->_arrayToString(array('first_name', 'last_name', 'spousename', 'email', 'phone_number', 'address_1', 'address_2', 'city', 'state', 'country', 'zip', 'listing_preference', 'names_list', 'wishes_to_remember', 'volunteering_for', 'adult_member_quantity', 'adult_member_subtotal', 'adult_nonmember_quantity', 'adult_nonmember_subtotal', 'staff_and_teacher_quantity', 'staff_and_teacher_subtotal', 'youth_member_quantity', 'youth_member_subtotal', 'youth_nonmember_quantity', 'youth_nonmember_subtotal', 'child_member_quantity', 'child_member_subtotal', 'child_nonmember_quantity', 'child_nonmember_subtotal', 'babysitting_quantity', 'babysitting_subtotal', 'yizkor_quantity', 'yizkor_subtotal', 'lulav_etrog_quantity', 'lulav_etrog_subtotal', 'payment_method', 'payment_amount', 'registered_date', 'paid', 'status')).$this->eol;
            break;
        }
		
		//Adults
		$adults = KFactory::get('admin::com.highholidays.model.adults')->getList();
		foreach($adults as $adult){
			switch ($state->filter_layout) {
                case 'filter_adults':
                    $this->output .= $this->_arrayToString(
                            array(
	        				    'first_name' => $adult->first_name,
	        				    'last_name' => $adult->last_name,
                				'email' => $adult->email,
								'address_1' => $adult->address_1,
								'address_2' => $adult->address_2,
								'city' => $adult->city,
								'state' => $adult->state,
								'country' => $adult->country,
								'zip' => $adult->zip,
                                'phone_number' => $adult->phone_1,
								'registrants_name' => $adult->registrants_fname.' '.$adult->registrants_lname
                            )
                        ).$this->eol;
                break;
			}
		}
		
		// Data
		foreach($rowset as $row) {
            switch ($state->filter_layout) {
                case 'filter_babysitting':
                    $this->output .= $this->_arrayToString(
                            array(
	        				    'first_name' => $row->first_name,
	        				    'last_name' => $row->last_name,
                				'email' => $row->email,
								'address_1' => $row->address_1,
								'address_2' => $row->address_2,
								'city' => $row->city,
								'state' => $row->state,
								'country' => $row->country,
								'zip' => $row->zip,
                                'phone_number' => ($row->area_code ? $row->area_code.'-' : '').$row->phone_1,
                                'number_of_babysitting_tickets_ordered' => $row->num_babysitting,
                                'payment_method' => $row->payment_method,
								'status' => $row->ismember ? 'member' : 'non-member'
                            )
                        ).$this->eol;
                break;
				case 'filter_members':
                    $this->output .= $this->_arrayToString(
                            array(
	        				    'first_name' => $row->first_name,
	        				    'last_name' => $row->last_name,
                				'email' => $row->email,
								'address_1' => $row->address_1,
								'address_2' => $row->address_2,
								'city' => $row->city,
								'state' => $row->state,
								'country' => $row->country,
								'zip' => $row->zip,
                                'phone_number' => ($row->area_code ? $row->area_code.'-' : '').$row->phone_1,
								'status' => $row->ismember ? 'member' : 'non-member'
                            )
                        ).$this->eol;
                break;
				case 'filter_nonmembers':
                    $this->output .= $this->_arrayToString(
                            array(
	        				    'first_name' => $row->first_name,
	        				    'last_name' => $row->last_name,
                				'email' => $row->email,
								'address_1' => $row->address_1,
								'address_2' => $row->address_2,
								'city' => $row->city,
								'state' => $row->state,
								'country' => $row->country,
								'zip' => $row->zip,
                                'phone_number' => ($row->area_code ? $row->area_code.'-' : '').$row->phone_1,
								'status' => $row->ismember ? 'member' : 'non-member'
                            )
                        ).$this->eol;
                break;
                case 'filter_yizkor':
                    $this->output .= $this->_arrayToString(
                            array(
	        				    'first_name' => $row->first_name,
	        				    'last_name' => $row->last_name,
                				'email' => $row->email,
								'address_1' => $row->address_1,
								'address_2' => $row->address_2,
								'city' => $row->city,
								'state' => $row->state,
								'country' => $row->country,
								'zip' => $row->zip,
								'phone_number' => ($row->area_code ? $row->area_code.'-' : '').$row->phone_1,
                                'listing_preference' => (!empty($row->yizkor_listing) ? JText::_('YIZKOR_LISTING_'.$row->yizkor_listing): ''),
                                'names_list' => $row->yizkor_names_list,
                                'wishes_to_remember' => $row->yizkor_wishes,
                                'payment_method' => $row->payment_method,
								'status' => $row->ismember ? 'member' : 'non-member'
                            )
                        ).$this->eol;
                break;
                case 'filter_lulavetrog':
                    $this->output .= $this->_arrayToString(
                            array(
	        				    'first_name' => $row->first_name,
	        				    'last_name' => $row->last_name,
								'email' => $row->email,
								'address_1' => $row->address_1,
								'address_2' => $row->address_2,
								'city' => $row->city,
								'state' => $row->state,
								'country' => $row->country,
								'zip' => $row->zip,
								'phone_number' => ($row->area_code ? $row->area_code.'-' : '').$row->phone_1,
                                'number_of_sets_ordered' => $row->num_lulavetrog,
                                'payment_method' => $row->payment_method,
								'status' => $row->ismember ? 'member' : 'non-member'
                            )
                        ).$this->eol;
                break;
                case 'filter_volunteer':
                    $volunteerfor = explode(',', $row->volunteer); $vfor = array();
                    foreach ($volunteerfor as $v) { $vfor[] = JText::_('VOLUNTEER_'.$v); }
                    $volunteerfor = implode(',',$vfor);

                    $this->output .= $this->_arrayToString(
                            array(
	        				    'first_name' => $row->first_name,
	        				    'last_name' => $row->last_name,
								'email' => $row->email,
								'address_1' => $row->address_1,
								'address_2' => $row->address_2,
								'city' => $row->city,
								'state' => $row->state,
								'country' => $row->country,
								'zip' => $row->zip,
								'phone_number' => ($row->area_code ? $row->area_code.'-' : '').$row->phone_1,
                                'volunteering_for' => $volunteerfor,
								'status' => $row->ismember ? 'member' : 'non-member'
                            )
                        ).$this->eol;                
                break;
                case 'filter_ticketorders':
                    $this->output .= $this->_arrayToString(
                            array(
	        				    'first_name' => $row->first_name,
	        				    'last_name' => $row->last_name,
                				'email' => $row->email,
								'address_1' => $row->address_1,
								'address_2' => $row->address_2,
								'city' => $row->city,
								'state' => $row->state,
								'country' => $row->country,
								'zip' => $row->zip,
                                'adult_member' => $row->num_adult_member,
                                'adult_nonmember' => $row->num_adult_nonmember,
                                'staff_and_teacher' => $row->num_staffteacher,
                                'youth_member' => $row->num_youth_member,
                                'youth_nonmember' => $row->num_youth_nonmember,
                                'child_member' => $row->num_child_member,
                                'child_nonmember' => $row->num_child_nonmember,
                                'babysitting' => $row->num_babysitting,
								'yizkor' => $row->num_yizkor,
								'lulav_etrog' => $row->num_lulavetrog,
								'status' => $row->ismember ? 'member' : 'non-member'
                            )
                        ).$this->eol;                
                break;
                case 'filter_billingdetails':
                    $this->output .= $this->_arrayToString(
                            array(
	        				    'first_name' => $row->first_name,
	        				    'last_name' => $row->last_name,
                				'email' => $row->email,
								'address_1' => $row->address_1,
								'address_2' => $row->address_2,
								'city' => $row->city,
								'state' => $row->state,
								'country' => $row->country,
								'zip' => $row->zip,
                                'phone_number' => ($row->area_code ? $row->area_code.'-' : '').$row->phone_1,
                                'payment_amount' => $row->total_amount,
                                'payment_method' => $row->payment_method,
								'status' => $row->ismember ? 'member' : 'non-member'
                            )
                        ).$this->eol;                
                break;
                case 'filter_all':				
					$volunteerfor = explode(',', $row->volunteer); $vfor = array();
                    foreach ($volunteerfor as $v) { $vfor[] = JText::_('VOLUNTEER_'.$v); }
                    $volunteerfor = implode(',',$vfor);
					
					$adult_member_subtotal = '';
					$adult_nonmember_subtotal = '';
					$staff_and_teacher_subtotal = '';
					$youth_member_subtotal = '';
					$youth_nonmember_subtotal = '';
					$child_member_subtotal = '';
					$child_nonmember_subtotal = '';
					$babysitting_subtotal = '';
					$yizkor_subtotal = '';
					$lulav_etrog_subtotal = '';
					$reg_selections = KFactory::tmp('admin::com.highholidays.model.registrationselections')->set('registration_id', $row->id)->getList();
					
					foreach($reg_selections as $reg_selection) {
							if($reg_selection->highholidays_selection_id == 1){
									$adult_member_subtotal = $reg_selection->sub_total;
							}	
							if($reg_selection->highholidays_selection_id == 2){
									$adult_nonmember_subtotal = $reg_selection->sub_total;
							}
							if($reg_selection->highholidays_selection_id == 3){
								$staff_and_teacher_subtotal = $reg_selection->sub_total;
							}
							if($reg_selection->highholidays_selection_id == 4){
								$youth_member_subtotal = $reg_selection->sub_total;
							}
							if($reg_selection->highholidays_selection_id == 5){
								$youth_nonmember_subtotal = $reg_selection->sub_total;
							}
							if($reg_selection->highholidays_selection_id == 6){
								$child_member_subtotal = $reg_selection->sub_total;
							}
							if($reg_selection->highholidays_selection_id == 7){
								$child_nonmember_subtotal = $reg_selection->sub_total;
							}
							if($reg_selection->highholidays_selection_id == 8){
								$babysitting_subtotal = $reg_selection->sub_total;
							}
							if($reg_selection->highholidays_selection_id == 9){
								$yizkor_subtotal = $reg_selection->sub_total;
							}
							if($reg_selection->highholidays_selection_id == 10){
								$lulav_etrog_subtotal = $reg_selection->sub_total;
							}	
					}
			
        			$this->output .= $this->_arrayToString(
        				array(
        				    'first_name' => $row->first_name,
        				    'last_name' => $row->last_name,
                            'spousename' => $row->spousename,
            				'email' => $row->email,
                            'phone_number' => ($row->area_code ? $row->area_code.'-' : '').$row->phone_1,
							'address_1' => $row->address_1,
							'address_2' => $row->address_2,
							'city' => $row->city,
							'state' => $row->state,
							'country' => $row->country,
							'zip' => $row->zip,
                            'listing_preference' => (!empty($row->yizkor_listing) ? JText::_('YIZKOR_LISTING_'.$row->yizkor_listing): ''),
                            'names_list' => $row->yizkor_names_list,
                            'wishes_to_remember' => $row->yizkor_wishes,
							'volunteering_for' => $volunteerfor,
                            'adult_member_quantity' => $row->num_adult_member,
							'adult_member_subtotal' => empty($adult_member_subtotal) ? '0.00' : $adult_member_subtotal,
                            'adult_nonmember_quantity' => $row->num_adult_nonmember,
							'adult_nonmember_subtotal' => empty($adult_nonmember_subtotal) ? '0.00' : $adult_nonmember_subtotal,
                            'staff_and_teacher_quantity' => $row->num_staffteacher,
							'staff_and_teacher_subtotal' => empty($staff_and_teacher_subtotal) ? '0.00' : $staff_and_teacher_subtotal,
                            'youth_member_quantity' => $row->num_youth_member,
							'youth_member_subtotal' => empty($youth_member_subtotal) ? '0.00' : $youth_member_subtotal,
                            'youth_nonmember_quantity' => $row->num_youth_nonmember,
							'youth_nonmember_subtotal' => empty($youth_nonmember_subtotal) ? '0.00' : $youth_nonmember_subtotal,
                            'child_member_quantity' => $row->num_child_member,
							'child_member_subtotal' => empty($child_member_subtotal) ? '0.00' : $child_member_subtotal,
                            'child_nonmember_quantity' => $row->num_child_nonmember,
							'child_nonmember_subtotal' => empty($child_nonmember_subtotal) ? '0.00' : $child_nonmember_subtotal,
                            'babysitting_quantity' => $row->num_babysitting,
							'babysitting_subtotal' => empty($babysitting_subtotal) ? '0.00' : $babysitting_subtotal,
							'yizkor_quantity' => $row->num_yizkor,
							'yizkor_subtotal' => empty($yizkor_subtotal) ? '0.00' : $yizkor_subtotal,
							'lulav_etrog_quantity' => $row->num_lulavetrog,
							'lulav_etrog_subtotal' => empty($lulav_etrog_subtotal) ? '0.00' : $lulav_etrog_subtotal,
                            'payment_method' => $row->payment_method,
                            'payment_amount'      => $row->total_amount,
                            'registered_date'     => $row->payment_date,
                            'paid'                => $row->paid ? 'Yes' : 'No',
							'status' => $row->ismember ? 'member' : 'non-member'
            			)
            		).$this->eol;
                break;
            }
		}
	 	
		return KViewFile::display();
	}

}