<?php

/**
* 
*/
class ComHighholidaysToolbarButtonReport extends KToolbarButtonDefault
{

    public function _initialize(KConfig $config)
    {
        $config->append(array(
            'text' => 'Run Reports',
            'icon' => 'icon-32-report'
        ));

        KFactory::get('lib.joomla.document')->addStyleDeclaration('.toolbar .icon-32-report { background-image: url('.JURI::root().'media/com_eventsdonations/images/icons/report.png); }');

        return parent::_initialize($config);
    }
	
	public function getLink()
	{
        return 'index.php?option=com_highholidays&view=registrations&layout=reports';
	}
}
