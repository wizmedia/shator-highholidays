<?php

/**
* 
*/
class ComHighholidaysToolbarButtonRegister extends KToolbarButtonDefault
{

    public function _initialize(KConfig $config)
    {
        $config->append(array(
            'text' => 'Register',
            'icon' => 'icon-32-register'
        ));

        KFactory::get('lib.joomla.document')->addStyleDeclaration('.toolbar .icon-32-register { background-image: url('.JURI::root().'media/com_eventsdonations/images/icons/register.png); }');

        return parent::_initialize($config);
    }

	public function getLink()
	{
        return 'index.php?option=com_highholidays&view=registration&layout=step1';
	}
}
