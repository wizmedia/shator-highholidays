<? defined('KOOWA') or die('Restricted access');?>

<div id="additional-adult-<?= $adultcount; ?>" class="hh_user_details_form additional_adults">

    <h4>Additional Adult #<?= $adultcount; ?></h4>

    <div><label for="adult-<?= $adultcount;?>-first_name">First name:* </label> <input type="text" name="adults[<?= $adultcount; ?>][first_name]" value="<?= $adult['first_name'];?>" size="30" class="required" id="adult-<?= $adultcount;?>-first_name" /></div>
    <div><label for="adult-<?= $adultcount;?>-last_name">Last name:* </label> <input type="text" name="adults[<?= $adultcount; ?>][last_name]" value="<?= $adult['last_name'];?>" size="30" class="required" id="adult-<?= $adultcount;?>-last_name" /></div>
    <div><label for="adult-<?= $adultcount;?>-address_1">Address: *</label> <input type="text" name="adults[<?= $adultcount; ?>][address_1]" value="<?= $adult['address_1'];?>" size="30" class="required" id="adult-<?= $adultcount;?>-address_1" /></div>
    <div><label for="adult-<?= $adultcount;?>-city">City: *</label> <input type="text" name="adults[<?= $adultcount; ?>][city]" value="<?= $adult['city'];?>" size="30" class="required" id="adult-<?= $adultcount;?>-city" /></div>
    <div><label for="adult-<?= $adultcount;?>-state">State: *</label> <input type="text" name="adults[<?= $adultcount; ?>][state]" value="<?= $adult['state'];?>" size="30" class="required" id="adult-<?= $adultcount;?>-state" /></div>
    <div><label for="adult-<?= $adultcount;?>-zip">Zip: *</label> <input type="text" name="adults[<?= $adultcount; ?>][zip]" value="<?= $adult['zip'];?>" size="30" class="required" id="adult-<?= $adultcount;?>-zip" /></div>
    <div><label for="adult-<?= $adultcount;?>-phone_1">Phone: *</label> <input type="text" name="adults[<?= $adultcount; ?>][phone_1]" value="<?= $adult['phone_1'];?>" size="30" class="required" id="adult-<?= $adultcount;?>-phone_1" /></div>
    <div><label for="adult-<?= $adultcount;?>-email">Email: *</label> <input type="text" name="adults[<?= $adultcount; ?>][email]" value="<?= $adult['email'];?>" size="30" class="required" id="adult-<?= $adultcount;?>-email" /></div>

    <input type="button" value="Remove" class="remove_adult" />
    <input class="add_adult" type="button" value="Add Another Adult">
</div>