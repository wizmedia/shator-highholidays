<style>
.adminlist { width: 100%;}
</style>

<?= @helper('behavior.mootools'); ?>

<style>
th { text-align: left; }
</style>

<script>
window.addEvent('domready', function() {

    var adminForm = $('adminForm');
    var hhTotalAmount = $('hh-total-amount');
    var hhTotalAmountText = $('hh-total-amount-text');

    var getTotalAmount = function() {
        var total = 0;
        var hhSubtotalAmount = adminForm.getElements('.hh-subtotal-amount');

        hhSubtotalAmount.each(function(item) {
           total += Number(item.value);
        });
        return total.toFixed(2);
    }


   $$('.remove-button').addEvent('click', function() {
       var selection_id = this.id.split('-')[2];
       var parent_tr = $('selection-'+selection_id);
		if(confirm('Are you sure you want to delete this selection?'))	{
            parent_tr.remove();
            totalAmount = getTotalAmount();
            hhTotalAmount.value = totalAmount;
            hhTotalAmountText.setHTML(totalAmount);
		}              
   });
});
</script>

<form action="<?= @route();?>" method="post" name="adminForm" id="adminForm">

<h3>Confirm your selections</h3>

<? /* Force to set orderselections value to 0 if not selected.  */ ?>
<? foreach ($selections as $selection): ?>
<input type="hidden" name="orderselections[<?= $selection->name;?>]" value="0" />
<? endforeach; ?>

<table class="adminlist">
    <tr>
        <th><?= @text('Title');?></th>
        <th width="100"><?= @text('Price');?></th>
        <th width="100"><?= @text('Quantity');?></th>
        <th width="100"><?= @text('Subtotal');?></th>
        <th></td>
    </tr>
<? $orderselections = $data->orderselections; ?>
<? $total = 0; ?>
<? foreach ($selections as $selection): ?>
	<? $orderselections[$selection->name] = (int)$orderselections[$selection->name]; ?>
    <? if (@!empty($orderselections[$selection->name])): ?>
        <? $quantity = $orderselections[$selection->name]; ?>
        <? $amount = $quantity * $selection->amount; ?>
        <? $price = $selection->amount; ?>
        <? $total += $amount; ?>
    <tr id="selection-<?= $selection->id; ?>">
        <td><?= $selection->title; ?></td>
        <td>$<?= $price; ?></td>
        <td><?= $quantity; ?></td>
        <td>$<?= number_format($amount, 2, '.', ''); ?></td>
        <td>
            <input type="hidden" value="<?= $amount;?>" class="hh-subtotal-amount" />
            <input type="hidden" name="orderselections[<?= $selection->name;?>]" value="<?= $quantity;?>" />
            <input type="button" value="Remove" class="remove-button" id="selection-button-<?= $selection->id;?>"/>
        </td>
    </tr>
    <? endif; ?>
<? endforeach; ?>
    <tr>
        <td colspan="3" align="right"><strong>Total</strong></td>
        <td>
            $<span id="hh-total-amount-text"><?= number_format($total, 2, '.', ''); ?></span>
            <input type="hidden" name="total_amount" disabled="disabled" value="<?= $total;?>" size="10" id="hh-total-amount" />
        </td>
        <td></td>
    </tr>
</table>

<a href="<?= @route('index.php?option=com_highholidays&view=registration'.(KFactory::get('lib.joomla.application')->isAdmin() ? '&layout=step1' : '')); ?>"><?= @text('Back'); ?></a>
<input type="hidden" name="step" value="step2" />
<input type="submit" value="Continue" />

</form>