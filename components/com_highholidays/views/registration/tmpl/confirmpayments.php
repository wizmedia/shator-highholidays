<? defined('KOOWA') or die('Restricted access'); ?>

<?= @helper('behavior.tooltip'); ?>
<?= @helper('behavior.mootools'); ?>
<script src="media://com_eventsdonations/js/paymentmethod.js" />
<style src="media://com_highholidays/css/default.css" />

<div id="highholidays">

    <form action="<?= @route();?>" method="post" name="adminForm" id="adminForm">

        <fieldset class="adminform">
            <legend><?= @text('Details'); ?></legend>

            <p><?= @text('Name'); ?>: <?= $data->first_name.' '.$data->last_name; ?></p>
            <p><?= @text('Spouse name'); ?>: <?= $data->spousename; ?></p>
            <p><?= @text('Email'); ?>: <?= $data->email; ?></p>

            <?= @template('selectionsform', array('selections' => $selections, 'orderselections' => $data->orderselections, 'add_hidden_total_amount' => TRUE)); ?>
        </fieldset>

		<fieldset class="adminform">
			<legend><?= @text('Payment Method'); ?></legend>

			<?= @helper('admin::com.highholidays.template.helper.select.paymentmethod', array('selected' => $data->payment_method)); ?>

		    <fieldset class="payment_method pm-cc">
		        <legend><?= @text('Card Info')?></legend>

		    	<table>
		    		<tr class="payment_method pm-cc">
		    			<td><?= @text('Credit Card'); ?>:</td>
		    			<td><?= @helper('admin::com.eventsdonations.template.helper.listbox.cctypes', array('name' => 'cctype')); ?></td>
		    		</tr>
		    		<tr class="payment_method pm-cc">
		    			<td><?= @text('Card Number'); ?>:</td>
		    			<td><input type="text" name="credit_card_number" size="50" /></td>
		    		</tr>
		    		<tr class="payment_method pm-cc">
		    			<td><?= @text('Credit Card Security Code'); ?>:</td>
		    			<td><input type="text" name="credit_card_code" size="50" /></td>
		    		</tr>
		    		<tr class="payment_method pm-cc">
		    			<td><?= @text('Exp. Date'); ?>:</td>
		    			<td>
		    				<?= @helper('admin::com.eventsdonations.template.helper.select.months', array('name' => 'exp_month')); ?>
		    				<?= @helper('admin::com.eventsdonations.template.helper.select.expirationyears', array('name' => 'exp_year')); ?>
		    			</td>
		    		</tr>
		        </table>
		    </fieldset>

		    <fieldset>
		        <legend><?= @text('Billing Info')?></legend>

		    	<table>
		    		<tr>
		    			<td>*<?= @text('Address 1'); ?>:</td>
		    			<td><input type="text" name="address_1" size="50" value="<?= $data->address_1;?>" /></td>
		    		</tr>
		    		<tr>
		    			<td><?= @text('Address 2'); ?>:</td>
		    			<td><input type="text" name="address_2" size="50" value="<?= $data->address_2;?>" /></td>
		    		</tr>
		    		<tr>
		    			<td>*<?= @text('City'); ?>:</td>
		    			<td><input type="text" name="city" size="50" value="<?= $data->city;?>" /></td>
		    		</tr>
		    		<tr>
		    			<td>*<?= @text('State/Province/Region'); ?>:</td>
		    			<td><input type="text" name="state" size="50" value="<?= $data->state;?>" /></td>
		    		</tr>
		    		<tr>
		    			<td>*<?= @text('Zip/Postal code'); ?>:</td>
		    			<td><input type="text" name="zip" size="50" value="<?= $data->zip;?>" /></td>
		    		</tr>
		    		<tr>
		    			<td><?= @text('Country'); ?>:</td>
		    			<td><input type="text" name="country" size="50" value="<?= $data->country;?>" /></td>
		    		</tr>
		    		<tr>
		    			<td>*<?= @text('Phone'); ?>:</td>
		    			<td><input type="text" name="area_code" size="4" value="<?= $data->area_code;?>" /><input type="text" name="phone_1" size="40" value="<?= $data->phone_1;?>" /></td>
		    		</tr>
		        </table>
		    </fieldset>

		</fieldset>


		

        <?= @template('admin::com.eventsdonations.view.list.customer_note', array('data' => $data)); ?>

        <input type="hidden" name="step" value="step3" />
    	<input type="submit" value="Complete Order" />
    </form>

</div>