<? defined('KOOWA') or die('Restricted access');?>

<?= @helper('behavior.mootools'); ?>

<style>
.hh_user_details_form label { display: inline-block; width: 150px; }
.additional_adults { margin-bottom: 10px; }
.order_selections { padding-left: 0;}
.order_selections li { list-style-type: none;}
.order_selections li span, .order_selections input { margin-right: 10px; }
</style>

<? JHTML::_('behavior.formvalidation'); ?>
<script language="javascript">
window.addEvent('domready', function() {   
   var adminForm = $('adminForm');
   adminForm.addEvent('submit', function(e) {
       if (!document.formvalidator.isValid(this)) {
           var e = new Event(e);
           e.stop();
       }
   });
  

   /* Add Adult */
   var adultsContainer = $('adults-container');
   var addAdultButton = $('add-adult');

   addAdultButton.addEvent('click', function(e) {
      addAdult(); 
   });

   $$('.remove_adult').addEvent('click', function(e) {
      removeAdult(this);
   });

   $$('.add_adult').addEvent('click', function(e) {
      addAdult();
   });

   function addAdult() {

       var adultNumber = adultsContainer.getChildren().length + 1;

       var row = new Element('div', {'id': 'additional-adult-'+adultNumber, 'class': 'hh_user_details_form additional_adults'});
       var removeButton = new Element('input', {'class': 'remove_adult', 'value': 'Remove', 'type': 'button'});
       var addButton = new Element('input', {'class': 'add_adult', 'value': 'Add Another Adult', 'type': 'button'});
       
       removeButton.addEvent('click', function(e) {
           removeAdult(this);
       });

       addButton.addEvent('click', function(e) {
          addAdult(); 
       });
       
       var html = '<h4>Additional Adult #'+adultNumber+'</h4>'
           + '<div><label for="adult-'+adultNumber+'-first_name">First name:* </label> <input type="text" name="adults['+adultNumber+'][first_name]" size="30" class="required" id="adult-'+adultNumber+'-first_name" /></div>'
           + '<div><label for="adult-'+adultNumber+'-last_name">Last name:* </label> <input type="text" name="adults['+adultNumber+'][last_name]" size="30" class="required" id="adult-'+adultNumber+'-last_name" /></div>'
           + '<div><label for="adult-'+adultNumber+'-address_1">Address: *</label> <input type="text" name="adults['+adultNumber+'][address_1]" size="30" class="required" id="adult-'+adultNumber+'-address_1" /></div>'
           + '<div><label for="adult-'+adultNumber+'-city">City: *</label> <input type="text" name="adults['+adultNumber+'][city]" size="30" class="required" id="adult-'+adultNumber+'-city" /></div>'
           + '<div><label for="adult-'+adultNumber+'-state">State: *</label> <input type="text" name="adults['+adultNumber+'][state]" size="30" class="required" id="adult-'+adultNumber+'-state" /></div>'
           + '<div><label for="adult-'+adultNumber+'-zip">Zip: *</label> <input type="text" name="adults['+adultNumber+'][zip]" size="30" class="required" id="adult-'+adultNumber+'-zip" /></div>'
           + '<div><label for="adult-'+adultNumber+'-phone_1">Phone: *</label> <input type="text" name="adults['+adultNumber+'][phone_1]" size="30" class="required" id="adult-'+adultNumber+'-phone_1" /></div>'
           + '<div><label for="adult-'+adultNumber+'-email">Email: *</label> <input type="text" name="adults['+adultNumber+'][email]" size="30" class="required validate-email" id="adult-'+adultNumber+'-email" /></div>';

        row.setHTML(html);
        removeButton.inject(row);
        addButton.inject(row);
        row.inject(adultsContainer);
   }

   var removeAdult = function(item) {
       item.getParent().remove();
   }
});
</script>

<div id="highholidays-order-form">

    <form action="<?= @route();?>" method="post" name="adminForm" id="adminForm" class="adminform form-validate">

    <h3>SHAARE TORAH HIGH HOLIDAY TICKET ORDER FORM</h3>

    <p>Thank you for planning to spend the High Holidays with Shaare Torah! Please fill out the following form to order your tickets.</p>
    <p><strong>Everyone who plans to attend High Holiday services must fill out this form. </strong></p>
    <p>High Holiday tickets are included with membership at Shaare Torah, but in order to get an accurate number of those planning to attend, everyone--members and nonmembers alike--must complete and submit this form.</p>

    <div class="hh_user_details_form">
    <div><label for="first_name">First name:* </label> <input type="text" name="first_name" value="<?= $data->first_name;?>" size="30" id="first_name" class="required" /></div>
    <div><label for="last_name">Last name:* </label> <input type="text" name="last_name" value="<?= $data->last_name;?>" size="30" id="last_name" class="required" /></div>
    <div><label for="spousename">Spouse / Partner name:</label> <input type="text" name="spousename" value="<?= $data->spousename;?>" size="30" id="spousename" /></div>
    <div><label for="address_1">Address: *</label> <input type="text" name="address_1" value="<?= $data->address_1;?>" size="30" id="address_1" class="required" /></div>
    <div><label for="city">City: *</label> <input type="text" name="city" value="<?= $data->city;?>" size="30" id="city" class="required" /></div>
    <div><label for="state">State: *</label> <input type="text" name="state" value="<?= $data->state;?>" size="30" id="state" class="required" /></div>
    <div><label for="zip">Zip: *</label> <input type="text" name="zip" value="<?= $data->zip;?>" size="30" id="zip" class="required" /></div>
    <div><label for="area_code">Phone: *</label> <input type="text" name="area_code" value="<?= $data->area_code;?>" size="2" id="area_code" class="required" /><input type="text" name="phone_1" value="<?= $data->phone_1;?>" size="22" id="phone_1" class="required" /></div>
    <div><label for="email">Email: *</label> <input type="text" name="email" value="<?= $data->email;?>" size="30" id="email" class="required validate-email" /></div>
    </div>

    <p><input type="checkbox" name="mail_me" value="1" checked="checked" /> Please add me to your email list so I can receive regular communications from Shaare Torah.</p>

    <p>Are you a Shaare Torah Member? 
        <select name="ismember">
            <option value="1" <?= $data->ismember ? 'selected' : ''; ?> >Yes</option>
            <option value="0" <?= $data->ismember ? '' : 'selected'; ?> >No</option>            
        </select>
    </p>

    <p>
        I am interested in learning more about joining Shaare Torah.
        <select name="interested_to_join">
            <option value="1" <?= $data->interested_to_join ? 'selected' : ''; ?> >Yes</option>
            <option value="0" <?= $data->interested_to_join ? '' : 'selected'; ?> >No</option>
        </select>
    </p>

    <p><strong>For security purposes, the name and contact information of every adult attending High Holiday Services must be collected. Please click here to add contact information of the additional adults attending:  <input type="button" value="Add Adult" id="add-adult" /></strong></p>    
    <? $adults = (array)KConfig::toData($data->adults); ?>

    <div id="adults-container">
        <? $i = 1;?>
        <? foreach ($adults as $adult): ?>
            <?= @template('userinfoform', array('adult' => $adult, 'adultcount' => $i)); ?>
            <? $i++; ?>
        <? endforeach; ?>
    </div>

    <h3>YIZKOR BOOK LISTINGS</h3>
    <p>A Yizkor (Remembrance) Book listing is a traditional way to honor the memory of those among your family and friends who have passed away. These books will be used Yom Kippur morning and throughout the year. We ask for a donation of $54 per listing; please indicate with a “1” in the order form at the bottom of this page.</p>

    <p>I would like my listing:</p>
    <ul>
        <li><label><input type="radio" name="yizkor_listing" value="none" id="yizkor-listing-none" <?= $data->yizkor_listing == 'none' ? 'checked="checked"' : ''; ?> /> <?= @text('YIZKOR_LISTING_NONE'); ?></label></li>
        <li><label><input type="radio" name="yizkor_listing" value="same" id="yizkor-listing-same" <?= $data->yizkor_listing == 'same' ? 'checked="checked"' : ''; ?> /> <?= @text('YIZKOR_LISTING_SAME'); ?></label></li>
        <?php /*<li><label><input type="radio" name="yizkor_listing" value="review" id="yizkor-listing-review" <?= $data->yizkor_listing == 'review' ? 'checked="checked"' : ''; ?> /> <?= @text('YIZKOR_LISTING_REVIEW'); ?></label></li>*/?>
        <li><label><input type="radio" name="yizkor_listing" value="new" id="yizkor-listing-new" <?= $data->yizkor_listing == 'new' ? 'checked="checked"' : ''; ?> /> <?= @text('YIZKOR_LISTING_NEW'); ?> (please enter information below)</label></li>
    </ul>

    <div>Your name(s) as you wish it to appear in the book: <br /><textarea name="yizkor_names_list" rows="5" cols="50"><?= $data->yizkor_names_list; ?></textarea></div>
    <div>Wishes to Remember: <br /><textarea name="yizkor_wishes" rows="5" cols="50"><?= $data->yizkor_wishes; ?></textarea></div>

    <h3>VOLUNTEER OPPORTUNITIES</h3>
    <p>I am interested in Volunteering to:</p>
    <p><small>Please check all that apply</small></p>
    <? $volunteer = (array)KConfig::toData($data->volunteer); ?>
    <ul>
        <li><label><input type="checkbox" name="volunteer[]" value="greeter" <?= in_array('greeter', $volunteer) ? 'checked="checked"' : ''; ?> /> <?= @text('VOLUNTEER_GREETER'); ?><label></li>
        <li><label><input type="checkbox" name="volunteer[]" value="service_participation" <?= in_array('service_participation', $volunteer) ? 'checked="checked"' : ''; ?> /> <?= @text('Participate in the Service (open ark, receive an aliyah, lead English reading, etc.)'); ?><label></li>
        <li><label><input type="checkbox" name="volunteer[]" value="setup" <?= in_array('setup', $volunteer) ? 'checked="checked"' : ''; ?> /> <?= @text('VOLUNTEER_SETUP'); ?><label></li>
        <li><label><input type="checkbox" name="volunteer[]" value="cleanup" <?= in_array('cleanup', $volunteer) ? 'checked="checked"' : ''; ?> /> <?= @text('VOLUNTEER_CLEANUP'); ?><label></li>
        <li><label><input type="checkbox" name="volunteer[]" value="children_volunteers" <?= in_array('children_volunteers', $volunteer) ? 'checked="checked"' : ''; ?> /> <?= @text('Volunteer in Children’s Programs'); ?><label></li>
    </ul>

    <h3>Order Form Selections:</h3>
    
    <ul class="order_selections">
        <li><p><strong><span>Quantity</span>Description</strong></p></li>
        <li><input type="text" name="orderselections[member-adult-tickets]" value="<?= $data->orderselections['member-adult-tickets']; ?>" size="5" /> Adult Member Tickets (Up to 2): $0</li>
        <li><input type="text" name="orderselections[nonmember-adult-tickets]" value="<?= $data->orderselections['nonmember-adult-tickets']; ?>" size="5" /> Additional or Non-Member Adult Tickets: $200/each</li>
        <li><input type="text" name="orderselections[staff-teacher-tickets]" value="<?= $data->orderselections['staff-teacher-tickets']; ?>" size="5" /> Staff/Teacher Tickets (Up to 2): $0</li>
        <li><input type="text" name="orderselections[member-youth-tickets]" value="<?= $data->orderselections['member-youth-tickets']; ?>" size="5" /> Member Youth Tickets (Ages 13-21): $0</li>
        <li><input type="text" name="orderselections[nonmember-youth-tickets]" value="<?= $data->orderselections['nonmember-youth-tickets']; ?>" size="5" /> Non-Member Youth Tickets (Ages 13-21): $75 each</li>
        <li><input type="text" name="orderselections[member-child-tickets]" value="<?= $data->orderselections['member-child-tickets']; ?>" size="5" /> Member Children’s Tickets (Ages 2-12): $0</li>
        <li><input type="text" name="orderselections[nonmember-child-tickets]" value="<?= $data->orderselections['nonmember-child-tickets']; ?>" size="5" /> Non-Member Children’s Tickets (Ages 2-12): $30/each</li>
        <li><input type="text" name="orderselections[babysitting-services]" value="<?= $data->orderselections['babysitting-services']; ?>" size="5" /> On Site Babysitting Services: (Ages 2-9): $35/child</li>
        <li><input type="text" name="orderselections[yizkor-book-listing]" value="<?= $data->orderselections['yizkor-book-listing']; ?>" size="5" /> Yizkor Book Listing Donation:  (Suggested donation of $54 for every 8 names)</li>
        <li><input type="text" name="orderselections[lulav-etrog]" value="<?= $data->orderselections['lulav-etrog']; ?>" size="5" /> Lulav &amp; Etrog Set: $40/set</li>
    </ul>
	
	<em style="font-size: 11px;display: block;margin-bottom: 5px;">Families who do not register for babysitting services, but who decide to use them, will be billed $54.</em>
    <input type="hidden" name="step" value="step1" />
    <input type="submit" value="Continue" />

    </form>

</div>