<? defined('KOOWA') or die('Restricted access');?>

<style>
th { text-align: left;}
</style>

<table class="adminlist" width="100%">
    <tr>
        <th><?= @text('Title'); ?></th>
        <th><?= @text('Price'); ?></th>
        <th><?= @text('Quantity'); ?></th>
        <th><?= @text('Sub Total'); ?></th>
    </tr>
    <? $total = 0; ?>
    <? foreach ($selections as $selection):?>
        <? if (@!empty($orderselections[$selection->name])): ?>
            <? $quantity = $orderselections[$selection->name]; ?>
            <? $amount = $quantity * $selection->amount; ?>
            <? $price = $selection->amount; ?>
            <? $total += $amount; ?>
        <tr>
            <td><?= $selection->title; ?></td>
            <td>$<?= $price; ?></td>
            <td><?= $quantity; ?></td>
            <td>$<?= number_format($amount, 2, '.', ''); ?></td>        
        </tr>
        <? endif; ?>
    <? endforeach; ?>
    <tr>
        <td colspan="3" align="right"><strong><?= @text('Total'); ?>:</strong> </td>
        <td>$<?= number_format($total, 2, '.', ''); ?></td>
    </tr>
</table>

<? if (isset($add_hidden_total_amount)): ?>
    <input type="hidden" name="total_amount" value="<?= $total;?>" />
<? endif; ?>