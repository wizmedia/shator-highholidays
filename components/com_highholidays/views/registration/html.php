<?php 

/**
* 
*/
class ComHighholidaysViewRegistrationHtml extends ComHighholidaysViewHtml
{
    
    public function display()
    {
		$data = KRequest::get('session.site::com.highholidays.controller.registration', 'raw', array());
        $data = new KConfig($data);

        $this->assign('data', $data);

        $selections = KFactory::tmp('admin::com.highholidays.model.selections')->limit(0)->getList();
        $this->assign('selections', $selections);

        return parent::display();
    }
}
