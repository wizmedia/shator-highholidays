<? defined('KOOWA') or die('Restricted access');?>

<p><strong>Thank you for spending the holidays with us at Shaare Torah.</strong></p>
<p>You will be receiving a confirmation email with the details of your order. If you have further comments or questions, please contact our office at 301-869-9842, or admin@shaaretorah.org.</p>

<p>L'shanah Tovah – Have a Wonderful 5772!</p>
