<? defined('KOOWA') or die('Restricted access'); ?>

<? $app = KFactory::get('lib.joomla.application'); ?>
<? $messages = $app->getMessageQueue(); ?>

<? $colors = array('message' => 'green', 'error' => 'red', 'notice' => 'blue'); ?>

<? foreach ($messages as $msg): ?>
    <p class="<?= $msg['type'];?>" style="color: <?= $colors[$msg['type']];?>"><?= $msg['message']; ?></p>
<? endforeach; ?>
