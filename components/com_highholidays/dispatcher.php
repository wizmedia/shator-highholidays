<?php 

/**
* 
*/
class ComHighholidaysDispatcher extends ComDefaultDispatcher
{
    protected function _actionDispatch(KCommandContext $context)
    {
        $params = JComponentHelper::getParams('com_highholidays');

        if ($params->get('offline', 0)) {
            $message = $params->get('offline_message', 'This section is temporarily down for maintenance. Please try again later.');
            return $message;
        }

        return parent::_actionDispatch($context);
    }
}
