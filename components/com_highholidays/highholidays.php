<?php

// Check if Koowa is active
if(!defined('KOOWA')) {
    JError::raiseWarning(0, JText::_("Koowa wasn't found. Please install the Koowa plugin and enable it."));
    return;
}
 
KFactory::map('site::com.highholidays.database.table.registrations', 'admin::com.highholidays.database.table.registrations');
KFactory::map('site::com.highholidays.database.table.registrationselections', 'admin::com.highholidays.database.table.registrationselections');

KFactory::get('lib.joomla.language')->load('com_eventsdonations');
KFactory::get('lib.joomla.language')->load('com_highholidays', JPATH_ADMINISTRATOR);

// Create the controller dispatcher
echo KFactory::get('site::com.highholidays.dispatcher')->dispatch();