<?php 

/**
* 
*/
class ComHighholidaysControllerRegistration extends ComDefaultControllerDefault
{
    
    public function __construct(KConfig $options)
    {
        parent::__construct($options);

        $this
            ->registerCallback('before.add', array(KFactory::get('admin::com.highholidays.helper.registration'), 'validateRegistration'))
            ->registerCallback('before.add', array(KFactory::get('admin::com.highholidays.helper.registration'), 'validateQuantity'))
            ->registerCallback('before.add', array(KFactory::get('admin::com.highholidays.helper.registration'), 'registrationsSteps'))
            ->registerCallback('before.add', array(KFactory::get('admin::com.eventsdonations.helper.payment'), 'processPayment'))
            ->registerCallback('after.add', array(KFactory::get('admin::com.highholidays.helper.registration'), 'saveSelections'))
            ->registerCallback('after.add', array(KFactory::get('admin::com.highholidays.helper.registration'), 'saveAdults'))
            ->registerCallback('after.add', array(KFactory::get('admin::com.highholidays.helper.registration'), 'sendMailNotification'))
            ->registerCallback('after.add', array(KFactory::get('admin::com.highholidays.helper.registration'), 'sendAdminMailNotification'))
            ->registerCallback('after.add', array(KFactory::get('admin::com.highholidays.helper.registration'), 'cleanSession'))
            ->registerCallback('after.add', array($this, 'overrideRedirect'))
            ;
    }

    public function overrideRedirect(KCommandContext $context)
    {
        $url = clone(KRequest::url());
        $url->scheme = 'http';
        $url->query = array('option' => 'com_highholidays', 'view' => 'messages', 'layout' => 'thankyou');		

        KFactory::get('lib.koowa.application')->redirect($url);
    }

    public function _actionRead(KCommandContext $context)
    {
        $url = KRequest::URL();
        $view = $this->getView();
        
        if ($view->getName() == 'registration' && $url->scheme != 'https') {
            $url->scheme = 'https';

            KFactory::get('lib.koowa.application')->redirect($url);
        }else {
            return parent::_actionRead($context);
        }
    }
}
